# 【维基链技术部2019年09月份月报】

## 应用建设
- CDP测试链公测上线，支持WICC质押获取稳定币WUSD
- DEX（去中心化交易所）测试链公测上线，支持WICC、WGRT的买卖交易
- 浏览器相关优化，兼容了多币种、CDP、DEX等相关内容
- 维基理财上线，给用户提供了WICC新的应用渠道
- 官网优化，更新海外模块，规划的SEO的优化方向
- 插件钱包升级v2.0，支持稳定币相关操作

## 公链建设
- 公链2.0版本全新升级，测试链公测上线，支持多币种、稳定币等多项功能


# 【09月06日周报】

## 上周工作总结

### 应用建设
- 维基理财开发与测试 (100%)
- 维基红包开发 (100%)
- javascript 钱包库bug修复（dex 卖单签名) (100%)
- kotlin语言钱包签名库文档 (100%)
- 插件钱包自定义节点功能整理 (100%)
- go 语言钱包库sdk文档 (100%)
- kotlin 资产发布交易 (100%)


### 公链建设 
- CDP 回归测试 (100%)
- DEX 自动化脚本编写 (70%)
- wasm 合约参数、数据abi反序列化 (100%)
- wasm 合约数据rpc查询 (100%)
- wasm VM更新 (100%)
- 增加根据前缀获取合约数据列表的RPC接口 (100%)
- 修复取消挂单交易未正确保存数据的 (100%)
- 修复获取所有挂单接口的最大数量参数的使用不正确 (100%)
- 增加LUA合约API：支持多币种的合约转账API (100%)
- 多币种转账交易添加收据信息（100%）
- 合约交易添加收据信息（100%）
- 投票交易支持免激活（100%）
- 修复 submitsendtx 解析参数导致段错误（100%）
- 移除合约交易输出缓存（100%）
- rpc 接口调整与帮助信息完善（100%）


### T2D2
- 区块链浏览器开发及bug修复 (100%)
- 生态钱包、交易所、应用的技术对接(三国、链盟、Beaxy、BitMart等) (100%)
- 出块时间投票应用上线，完成管理员批量赎回抵押功能开发 (100%)
- 公链 2.0 RPC, 更新开发者文档 (90%)


### 稳定币应用层
- CDP 应用层测试 (95%)
- DEX 应用开层开发 (95%)

## 本周工作规划

### 应用建设
- 维基红包测试和上线
- 维基时代埋点及钱包功能优化（添加稳定币部分）原型设计完成
- 钱包库支持资产发布、更新，多币种合约部署、调用交易接口


### 公链建设
- 公链安全性、性能测试
- wasm 合约多层收据及abi反序列化
- wasm abi白盒
- wasm 合约授权
- 完成LUA合约API：支持多币种的合约转账API、RPC
- 测试反馈/联调发现的 bug

### T2D2
- 测试升级插件钱包
- 区块链浏览器开发与上线

### 稳定币应用层
- DEX 应用层开发
- CDP、DEX 应用层测试


# 【09月13日周报】

## 上周工作总结

### 应用建设
- 维基红包发布上线 (100%)
- javascript 钱包库（资产发布，资产更新）(100%)
- kotlin语言钱包签名库（资产发布，资产更新，多币种合约调用，lua合约部署）(100%)
- go语言钱包签名库（资产发布，资产更新,多币种合约调用，多币种合约部署）(100%)
- JsBridge 2.0 接口整理  (50%)
- 维基时代v2.1.0：钱包添加WUSD、WGRT、增加多币种转账接口、应用中心调用接口等优化功能，原型设计 (70%)


### 公链建设 
- 支持多币种转账。(100%)
- DEX 自动化脚本编写 (70%)
- 修改结算交易中对成交金额的检查。（100%）
- 修改发布资产的符号格式限制。（100%）
- 修正bug：发布资产交易对资产拥有者的检查有误。（100%）
- 修正bug：挂单RPC接口对不存在的币种提示有误。（100%）
- 增加LUA合约API：获取合约交易转入的币种和金额。（100%）
- 增加LUA合约API：获取根据币种获取账户余额。（100%）
- 多币种合约交易部署、调用 rpc (100%)
- 修复交易防重缓存导致区块回滚失败 (100%)
- 修复喂价交易缓存错误导致区块回滚失败 (100%)
- submitsendtx rpc 根据当前高度兼容新旧转账交易 (100%)
- 支持交易（转账、合约调用、稳定币相关交易）免激活 (100%)
- Importprivkey rpc 支持导入矿工私钥，实现冷挖矿 (100%)
- 需要使用钱包的 rpc 检测钱包是否已锁定 (100%)
- 合约交易消耗的 gas（WUSD）分配给风险备用金 (100%)
- 协助升级 testnet (100%)
- wasm合约多层收据 (100%)
- wasm合约多层收据abi反序列化 (100%)
- wasm exception 体系 (100%)
- wasm abi白盒设计和边界防御 (50%)
- bft共识机制功能代码开发 (100%)
- btf机制所涉及的rpc接口状态获取方面的功能修改(待测试) (50%)
- 共识机制稳定性测试，解决与原有机制对接产生的bug (50%)


### T2D2
- 区块链浏览器测试链上线 (100%)
- 梦想基金需求沟通、技术框架设计 (100%)
- WRC721资料准备 (90%)


### 稳定币应用层
- CDP 应用层开发 (100%)
- DEX 应用开层开发 (100%)

## 本周工作规划

### 应用建设
- 维基时代v2.1.0 原型设计 (100%)
- JsBridge 2.0 接口整理
- 钱包库文档更新

### 公链建设
- 稳定币持续优化修复
- wasm abi白盒
- wasm 合约权限和计费
- 推进共识机制的调试，与原有其他功能的对接，并在test网络进行小范围测试

### T2D2
- 维基生态上交易所、钱包、应用的公链测试网升级公告，技术对接
- 开始梦想基金合约开发、测试多币种调用合约接口
- 推动整个梦想基金技术进度

### 稳定币应用层
- CDP、DEX 持续测试优化


# 【09月20日周报】

## 上周工作总结

### 应用建设
- 本周维基应用层多项工作取得进展
	- 梦想基金合约开发	(100%)
	- 沟通了官网SEO新方案 （100%）
	- 维基官网新功能开发  （80%）
	- 维基理财第二期开发  （70%）

### 公链建设 
- 新版本“腾”联调，修改了10个bug，公链更健壮 （100%） 
	- CDP 抵押、赎回、清算、强制清算相关的 bug
	- submitcoinstaketx 参数解析错误
	- 全局抵押数量、全局贷出数量溢出
	- 最新节点程序同步主网数据失败
	- 投票收益新公式的计算错误
	- 注册账户交易中未正确使用公钥
	- 合约调用所产生的收据没有被正确保存
	- 转账交易无法给自己转账
	- 合约转账接口无法正确识别转账地址
	- 系统挂单导致程序崩溃
	- wasm虚拟机接入取得新进展，完成了多项功能 （100%）
	- abi 白盒测试编写完成
	- abi 结构递归DAG防御
	- abi json数组替代结构
	- api 浮点数交换
- 调整和新实现了一些功能 （100%）
	- 调整投票获取的增发 WICC 收益计算公式 
	- 修改CDP抵押交易以支持多币种抵押
	- 为收据增加收据编号以改进其存储和显示
	- 实现了aBFT共识机制


### T2D2
- 随公链改动更新开发者文档2.0  100%


### 稳定币应用层
- CDP 应用层联调联测 (100%)
- DEX 应用层联调联测 (100%)


## 本周工作规划

### 应用建设
- WRC-30资产发布功能Web端，原型设计100%
- WRC-30资产发布功能移动端，原型设计20%
- 维基时代后端修改
- 维基理财: 完成新合约解析, 提前解仓分红
- 官网: 完成API开发, 开始与前端联调

### 公链建设
- 稳定币持续优化修复
- wasm api白盒
- wasmio.bank native 合约
- wasmio native 合约abi
- 开展跨链功能前期工作

### T2D2
- 推动插件钱包v2.0(支持稳定币) 公测上线
- 对接OTC需求
- 维基生态上交易所、钱包、应用的公链测试网升级公告，技术对接

### 稳定币应用层
- CDP、DEX 持续测试优化

# 【09月27日周报】

## 上周工作总结

### 公链建设 
- 新版本联调，bug修复，公链更健壮（100%） 
    - 解决因为分叉导致无法同步区块的问题。(100%)
    - 解决合约多账户转账接口参数解析错误的问题。（100%）
    - 解决合约调用交易未正确获取当前块的燃料率的问题。（100%）
    - 修复喂价缓存导致回滚失败的 bug (100%)
    - 修复修改账户余额时未对操作结果判断导致的余额异常 (100%)
    - 修复 CDP 缓存未处理区块回滚导致数据异常 (100%)
  
- 调整和新实现了一些功能 
    - 完善发布资产交易对发布人和拥有者的账户限制。（100%）
    - 调整中位数交易在区块中的位置，完善矿工执行中位数交易逻辑 (100%)
    - 完善系统强制清算的收据信息 (100%)
    - wasm api exception （100%）
    - wasm  boost环境测试（100%）
    - wasm api 白盒 （80%）

### 应用建设
- 维基官网优化（100%）
- 维基理财第二期开发  （90%）
- WRC-30资产发布功能Web端，原型设计(100%)
- 插件钱包v2.0(支持稳定币) 公测上线（100%）
- OTC需求准备
- 维基生态技术对接（交易所、钱包、应用的公链）

### 稳定币应用层
- 浏览器稳定币相关修改（100%）
- CDP 、DEX测试链公测上线

## 本周工作规划

### 公链建设
- 公链主网升级
- wasm 白盒
- wasm.bank 
- cosmos等跨链调查

### 应用建设
- 生态应用主网升级
- 维基时代v2.1开发和测试
- 维基时代v3.0原型设计（多链钱包）
- WRC-30资产发布功能移动端，原型设计

### 稳定币应用层
- CDP、DEX 持续优化
- 维基时代2.1版本开发

